#ifndef EXAMPLE_DEFS_H
#define EXAMPLE_DEFS_H

#include "hd44780.h"

#if defined              STM32F1
extern I2C_HandleTypeDef hi2c1;
#define I2C &hi2c1
#endif

#endif // EXAMPLE_DEFS_H
