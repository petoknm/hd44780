#include "example.h"
#include "example_defs.h"
#include "gpio_plugin_direct.h"
#include "gpio_plugin_pcf8574.h"
#include "gpio_plugin_pin_swap.h"

static void error() {
    while (1) {}
}

static void display_something(hd44780* lcd) {
    lcd_clear_display(lcd);
    lcd_set_location(lcd, 0, 0);
    lcd_write_string(lcd, "pi:");
    lcd_set_location(lcd, 0, 1);
    lcd_write_string(lcd, "e:");

    while (1) {
        lcd_set_location(lcd, 6, 0);
        lcd_write_float(lcd, 3.1415926, 7);
        lcd_set_location(lcd, 6, 1);
        lcd_write_float(lcd, 2.71, 2);
        HAL_Delay(3000);
    }
}

/**
 * Example using the direct GPIO plugin
 */
// void example() {
//     hd44780        lcd;
//     hd44780_config cfg;
//     cfg.lines   = DISPLAY_LINES_2;
//     cfg.timeout = 100;
//     cfg.gpio    = lcd_plugin_direct(GPIOC);
//
//     if (lcd_init(&cfg, &lcd) != LCD_OK) error();
//
//     display_something(&lcd);
// }

/**
 * Example using the PCF8574 GPIO plugin
 */
// void example() {
//     pcf8574 pcf;
//     pcf.address = 7;
//     pcf.timeout = 100;
//     pcf.i2c     = I2C;
//
//     if (pcf8574_init(&pcf) != PCF8574_OK) error();
//
//     hd44780        lcd;
//     hd44780_config cfg;
//     cfg.lines   = DISPLAY_LINES_2;
//     cfg.timeout = 100;
//     cfg.gpio    = lcd_plugin_pcf8574(&pcf);
//
//     if (lcd_init(&cfg, &lcd) != LCD_OK) error();
//
//     display_something(&lcd);
// }

/**
 * Example using the PCF8574 GPIO plugin and pin_swap plugin
 */
void example() {
    pcf8574 pcf;
    pcf.address = 7;
    pcf.timeout = 100;
    pcf.i2c     = I2C;

    if (pcf8574_init(&pcf) != PCF8574_OK) error();

    LCD_PIN layout[8] = {LCD_PIN_LED, LCD_PIN_E,  LCD_PIN_RW, LCD_PIN_RS,
                         LCD_PIN_D4,  LCD_PIN_D5, LCD_PIN_D6, LCD_PIN_D7};

    pin_swap_config swap_config =
        lcd_plugin_pin_swap_create(lcd_plugin_pcf8574(&pcf), layout);

    hd44780        lcd;
    hd44780_config cfg;
    cfg.lines   = DISPLAY_LINES_2;
    cfg.timeout = 100;
    cfg.gpio    = lcd_plugin_pin_swap(&swap_config);

    if (lcd_init(&cfg, &lcd) != LCD_OK) error();

    display_something(&lcd);
}
