var indexSectionsWithContent =
{
  0: "bcdfghlprstw",
  1: "ghp",
  2: "gh",
  3: "l",
  4: "bcdfglprstw",
  5: "l",
  6: "dfls"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator"
};

