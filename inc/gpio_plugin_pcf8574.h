#ifndef GPIO_PLUGIN_PCF8574_H
#define GPIO_PLUGIN_PCF8574_H

#include "hd44780.h"
#include "pcf8574.h"

gpio_plugin lcd_plugin_pcf8574(pcf8574* pcf);

#endif // GPIO_PLUGIN_PCF8574_H
