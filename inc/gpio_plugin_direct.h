#ifndef GPIO_PLUGIN_DIRECT_H
#define GPIO_PLUGIN_DIRECT_H

#include "hd44780.h"

gpio_plugin lcd_plugin_direct(GPIO_TypeDef* port);

#endif // GPIO_PLUGIN_DIRECT_H
