#ifndef HD44780_H
#define HD44780_H

#if defined STM32F1
#include "stm32f1xx_hal.h"
#elif defined STM232F4
#include "stm32f4xx_hal.h"
#endif

#include <stdbool.h>

/**
 * GPIO plugin definition
 */
typedef struct {
    void (*write)(uint8_t, void*);
    uint8_t (*read)(void*);
    void* data;
} gpio_plugin;

/**
 * LCD configuration structure
 */
typedef struct {
    uint8_t     lines;
    uint32_t    timeout;
    gpio_plugin gpio;
} hd44780_config;

/**
 * LCD handle structure
 */
typedef struct {
    uint8_t        state;
    hd44780_config config;
} hd44780;

/**
 * Possible return values for the functions
 */
typedef enum {
    /**
     * Function call was successful
     */
    LCD_OK,

    /**
     * Function call timed out
     */
    LCD_TIMEOUT,

    /**
     * Function call was unsuccessful
     */
    LCD_ERROR
} LCD_RESULT;

/**
 * Enumeration of the LCD pins
 */
typedef enum {
    LCD_PIN_D4  = 0,
    LCD_PIN_D5  = 1,
    LCD_PIN_D6  = 2,
    LCD_PIN_D7  = 3,
    LCD_PIN_RS  = 4,
    LCD_PIN_RW  = 5,
    LCD_PIN_E   = 6,
    LCD_PIN_LED = 7
} LCD_PIN;

typedef enum { DATA_LENGTH_4 = 0, DATA_LENGHT_8 = 1 } LCD_DATA_LENGTH;

typedef enum { DISPLAY_LINES_1 = 0, DISPLAY_LINES_2 = 1 } LCD_DISPLAY_LINES;

typedef enum { FONT_5x8 = 0, FONT_5x10 = 1 } LCD_FONT;

/**
 * LCD direction
 */
typedef enum { DIRECTION_LEFT = 0, DIRECTION_RIGHT = 1 } LCD_DIRECTION;

/**
 * LCD direction increment
 */
typedef enum {
    DIRECTION_INCREMENT = 1,
    DIRECTION_DECREMENT = 2
} LCD_DIRECTION_INC_DEC;

/**
 * LCD shift
 */
typedef enum { SHIFT_YES = 1, SHIFT_NO = 0 } LCD_SHIFT;

/**
 *	LCD initialization function
 *	@param  config - a pointer to the LCD configuration structure
 *	@param  handle - a pointer to the LCD handle
 *	@return whether the function was successful or not
 */
LCD_RESULT lcd_init(hd44780_config* config, hd44780* handle);

/**
 * Sets the location of the memory pointer in the controller (used to control
 * other operations (for example where to write a string))
 * @param   handle - a pointer to the LCD handle
 * @param   x - x-coordinate of the location
 * @param   y - y-coordinate of the location
 * @return  whether the function was successful or not
 */
LCD_RESULT lcd_set_location(hd44780* handle, uint8_t x, uint8_t y);

/**
 * Writes a string to the LCD
 * @param   handle - a pointer to the LCD handle
 * @param   s - string you want to write to the LCD
 * @return  whether the function was successful or not
 */
LCD_RESULT lcd_write_string(hd44780* handle, char* s);

/**
 * Clears the LCD
 * @param   handle - a pointer to the LCD handle
 * @return  whether the function was successful or not
 */
LCD_RESULT lcd_clear_display(hd44780* handle);

/**
 * Controls the state of the LCD backlight
 * @param   handle - a pointer to the LCD handle
 * @param   on - set it to 1 if you want to turn the backlight on, else 0
 * @return  whether the function was successful or not
 */
LCD_RESULT lcd_set_led(hd44780* handle, bool on);

LCD_RESULT lcd_display_ctrl(hd44780* handle, bool display_on, bool cursor_on,
                            bool cursor_blinking);

/**
 * Shifts the cursor in the specified direction certain number of steps
 * @param   handle - a pointer to the LCD handle
 * @param   direction - specifies the direction
 * @param   steps - specifies how many positions to shift the cursor by
 * @return  whether the function was successful or not
 */
LCD_RESULT lcd_shift_cursor(hd44780* handle, LCD_DIRECTION direction,
                            uint8_t steps);

/**
 * Shifts the contents of the LCD
 * @param   handle - a pointer to the LCD handle
 * @param   direction - directions of the shift
 * @param   steps - how many positions to shift the contents by
 * @return  whether the function was successful or not
 */
LCD_RESULT lcd_shift_display(hd44780* handle, LCD_DIRECTION direction,
                             uint8_t steps);

/**
 * Writes a number to the LCD
 * @param   handle - a pointer to the LCD handle
 * @param   n - a number you want to write to the LCD
 * @param   base - numeric base to display the number in
 * @return  whether the function was successful or not
 */
LCD_RESULT lcd_write_number(hd44780* handle, unsigned long n, uint8_t base);

LCD_RESULT lcd_write_float(hd44780* handle, double number, uint8_t digits);

/**
 * Sets the mode by which data is written to the LCD
 * @param   handle - a pointer to the LCD handle
 * @param   direction
 * @param   shift
 * @return  whether the function was successful or not
 */
LCD_RESULT lcd_set_entry_mode(hd44780* handle, LCD_DIRECTION_INC_DEC direction,
                              LCD_SHIFT shift);

#endif /* HD44780_H */
