#ifndef GPIO_PLUGIN_PIN_SWAP_H
#define GPIO_PLUGIN_PIN_SWAP_H

#include "hd44780.h"

typedef struct {
    gpio_plugin plugin;

    /**
     * Mapping from the library layout to custom one.
     * Indices are LCD_PINs from the library layout.
     * Values are corresponding LCD_PINs in the custom layout.
     */
    LCD_PIN forward[8];

    /**
     * Mapping from custom layout to the library one.
     * Indices are LCD_PINs from the custom layout.
     * Values are corresponding LCD_PINs in the library layout.
     */
    LCD_PIN backward[8];
} pin_swap_config;

/**
 * Creates a pin swap configuration needed for the plugin
 * @param  plugin plugin to wrap
 * @param  layout new LCD_PIN layout (pointer to 8 LCD_PINs)
 * @return        pin swap configuration
 */
pin_swap_config lcd_plugin_pin_swap_create(gpio_plugin plugin, LCD_PIN* layout);

/**
 * Creates a swap plugin from a configuration
 */
gpio_plugin lcd_plugin_pin_swap(pin_swap_config* config);

#endif // GPIO_PLUGIN_PIN_SWAP_H
