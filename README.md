[![Build Status](https://travis-ci.org/petoknm/HD44780.svg?branch=master)](https://travis-ci.org/petoknm/HD44780)

# HD44780
Driver for HD44780 displays for STM32

## Documentation
Check out the [generated documentation here](https://github.com/petoknm/HD44780/blob/master/api.md).

## GPIO plugins
This library lets you define how to handle low level GPIO. Some want to use GPIO
directly and some want to use something like PCF8574 I2C GPIO expander. You can
provide your own GPIO handler that is responsible of reading and writing to the
GPIO.

Built-in GPIO plugins:
 - `direct` (STM32 HAL GPIO)
 - [`pcf8574`](https://github.com/petoknm/PCF8574) (over I2C GPIO expander)
 - `pin_swap` (wrapper for any plugin that swaps the pin layout)

### `pin_swap` plugin
All data the library handles has to follow a specific format. The bytes being written to or read from the GPIO have to match the following bit order:
```
LCD_PIN     | bit
------------+----
LCD_PIN_D4  | 0
LCD_PIN_D5  | 1
LCD_PIN_D6  | 2
LCD_PIN_D7  | 3
LCD_PIN_RS  | 4
LCD_PIN_RW  | 5
LCD_PIN_E   | 6
LCD_PIN_LED | 7
```
If your pin mapping is different than the one library is expecting, you can use
the `pin_swap` plugin. To configure the `pin_swap` plugin you just specify
the plugin to wrap and the new layout you want.

e.g.:
```c
LCD_PIN new_layout[8] = {
    LCD_PIN_LED,
    LCD_PIN_E,
    LCD_PIN_RW,
    LCD_PIN_RS,
    LCD_PIN_D4,
    LCD_PIN_D5,
    LCD_PIN_D6,
    LCD_PIN_D7
};

pin_swap_config swap_config =
    lcd_plugin_pin_swap_create(some_other_gpio_plugin, new_layout);

gpio_plugin plugin = lcd_plugin_pin_swap(&swap_config);
```
This causes the wrapped plugin (in this example `some_other_gpio_plugin`) to
see the data transformed into the new layout when writing and when reading the
data will be transformed back into the library format as well.

So this way you can use for example PCF8574 I2C GPIO expander with any pin
routing to the HD44780 just by using the `pcf8574` plugin and wrapping it in
this `pin_swap` plugin.

\* WARNING: the new layout array has to contain all the `LCD_PIN`s exactly
once, otherwise the behaviour is undefined.

## Example
Check out the [general example](https://github.com/petoknm/HD44780/tree/master/example/general)
which is used by every microcontroller specific example.

## Requirements:
For building you will need:
 - make
 - arm-none-eabi-gcc

For developing you will also need:
 - clang-format
 - doxygen
 - graphviz
 - moxygen

## Building examples
```shell
# In the root folder of this repo
# To download dependencies like the PCF8574 library
make dependencies
# In one of the example folders (e.g. example/stm32f1)
# cd example/stm32f1
make
```

## Updating documentation
```shell
make docs
```

## Formatting sources
```shell
make format
```
