#include "hd44780.h"

static void write(uint8_t data, void* gpio_port) {
    uint16_t v = ((GPIO_TypeDef*)gpio_port)->ODR;
    v &= 0xFF00;
    v |= data;
    ((GPIO_TypeDef*)gpio_port)->ODR = v;
}

static uint8_t read(void* gpio_port) { return ((GPIO_TypeDef*)gpio_port)->IDR; }

gpio_plugin lcd_plugin_direct(GPIO_TypeDef* gpio_port) {
    return (gpio_plugin){write, read, (void*)gpio_port};
}
