#include "hd44780.h"
#include <math.h>

/**
 * Rewrites a bit in the state variable with the value specified
 * @param   handle - a pointer to the LCD handle
 * @param   value - value of the bit (0 or 1)
 * @param   pin - pin which you want to write to
 */
static void state_write_bit(hd44780* handle, bool value, LCD_PIN pin) {
    if (value) {
        handle->state |= 1 << pin;
    } else {
        handle->state &= ~(1 << pin);
    }
}

/**
 * Writes lower 4bits of data to the state
 * @param   handle - a pointer to the LCD handle
 * @param   data - data you want to put on the data bus (lower 4bits)
 */
static void state_write_data(hd44780* handle, uint8_t data) {
    state_write_bit(handle, data & 0x1, LCD_PIN_D4);
    state_write_bit(handle, data & 0x2, LCD_PIN_D5);
    state_write_bit(handle, data & 0x4, LCD_PIN_D6);
    state_write_bit(handle, data & 0x8, LCD_PIN_D7);
}

#define GPIO_WRITE() \
    handle->config.gpio.write(handle->state, handle->config.gpio.data)

static bool get_busy_flag(hd44780* handle) {
    state_write_bit(handle, 0, LCD_PIN_E);
    state_write_bit(handle, 0, LCD_PIN_RS);
    state_write_bit(handle, 1, LCD_PIN_RW);
    GPIO_WRITE();

    state_write_bit(handle, 1, LCD_PIN_E);
    GPIO_WRITE();

    uint8_t dh = handle->config.gpio.read(handle->config.gpio.data);

    state_write_bit(handle, 0, LCD_PIN_E);
    GPIO_WRITE();

    state_write_bit(handle, 1, LCD_PIN_E);
    GPIO_WRITE();

    // uint8_t dl = handle->config.gpio.read();

    state_write_bit(handle, 0, LCD_PIN_E);
    state_write_bit(handle, 0, LCD_PIN_RW);
    GPIO_WRITE();

    return (dh & LCD_PIN_D7) == LCD_PIN_D7;
}

/**
 * Waits until the busy flag is reset or timeout is reached
 * @param   handle - a pointer to the LCD handle
 * @return  whether the function was successful or not
 */
static LCD_RESULT wait_for_busy_flag(hd44780* handle) {
    uint32_t startTick = HAL_GetTick();
    while (get_busy_flag(handle)) {
        if (HAL_GetTick() - startTick >= handle->config.timeout)
            return LCD_TIMEOUT;
    }
    return LCD_OK;
}

static LCD_RESULT write_byte(hd44780* handle, bool rs, uint8_t val) {
    state_write_bit(handle, 0, LCD_PIN_E);
    state_write_bit(handle, rs, LCD_PIN_RS);
    GPIO_WRITE();

    state_write_data(handle, val >> 4);
    state_write_bit(handle, 1, LCD_PIN_E);
    GPIO_WRITE();

    state_write_bit(handle, 0, LCD_PIN_E);
    GPIO_WRITE();

    state_write_data(handle, val);
    state_write_bit(handle, 1, LCD_PIN_E);
    GPIO_WRITE();

    state_write_bit(handle, 0, LCD_PIN_E);
    GPIO_WRITE();

    return wait_for_busy_flag(handle);
}

/**
 * Sends a command to the HD44780 controller
 * @param   handle - a pointer to the LCD handle
 * @param   cmd - a command you want to send
 * @return  whether the function was successful or not
 */
static LCD_RESULT write_cmd(hd44780* handle, uint8_t data) {
    return write_byte(handle, 0, data);
}

/**
 * Sends data to the HD44780 controller
 * @param   handle - a pointer to the LCD handle
 * @param   data - data you want to send
 * @return  whether the function was successful or not
 */
static LCD_RESULT write_data(hd44780* handle, uint8_t data) {
    return write_byte(handle, 1, data);
}

static LCD_RESULT function_set(hd44780* handle, LCD_DATA_LENGTH dl,
                               LCD_DISPLAY_LINES n, LCD_FONT f) {
    uint8_t cmd = 0x20 | (dl << 4) | (n << 3) | (f << 2);
    return write_cmd(handle, cmd);
}

LCD_RESULT lcd_init(hd44780_config* config, hd44780* handle) {
    handle->config = *config;

    HAL_Delay(50);

    // Initialization by instruction
    handle->state = 0;
    // state_write_bit(handle, 0, LCD_PIN_RS);
    // state_write_bit(handle, 0, LCD_PIN_RW);
    // state_write_bit(handle, 0, LCD_PIN_E);
    GPIO_WRITE();

    state_write_data(handle, 3);
    GPIO_WRITE();

    state_write_bit(handle, 1, LCD_PIN_E);
    GPIO_WRITE();
    HAL_Delay(1);
    state_write_bit(handle, 0, LCD_PIN_E);
    GPIO_WRITE();
    HAL_Delay(5);

    // state_write_data(handle, 3);

    state_write_bit(handle, 1, LCD_PIN_E);
    GPIO_WRITE();
    HAL_Delay(1);
    state_write_bit(handle, 0, LCD_PIN_E);
    GPIO_WRITE();
    HAL_Delay(1);

    // state_write_data(handle, 3);

    state_write_bit(handle, 1, LCD_PIN_E);
    GPIO_WRITE();
    HAL_Delay(1);
    state_write_bit(handle, 0, LCD_PIN_E);
    GPIO_WRITE();
    HAL_Delay(1);

    state_write_data(handle, 2);
    GPIO_WRITE();

    state_write_bit(handle, 1, LCD_PIN_E);
    GPIO_WRITE();
    HAL_Delay(1);
    state_write_bit(handle, 0, LCD_PIN_E);
    GPIO_WRITE();
    HAL_Delay(1);

    function_set(handle, DATA_LENGTH_4, DISPLAY_LINES_2, FONT_5x8);

    lcd_display_ctrl(handle, false, false, false);

    lcd_clear_display(handle);

    lcd_set_entry_mode(handle, DIRECTION_INCREMENT, SHIFT_NO);

    lcd_set_led(handle, true);

    return LCD_OK;
}

LCD_RESULT lcd_set_location(hd44780* handle, uint8_t x, uint8_t y) {
    uint8_t address = 0;
    switch (handle->config.lines) {
    case 1:
        address = x;
        break;
    case 2:
        address = 0x40 * y + x;
        break;
    case 4:
        if (y >= 3) address += 0x10;
        if (y & 1) address += 0x40;
        address += x;
        break;
    default:
        return LCD_ERROR;
    }
    uint8_t cmd = 0x80 | address;
    wait_for_busy_flag(handle);
    return write_cmd(handle, cmd);
}

LCD_RESULT lcd_write_string(hd44780* handle, char* s) {
    while (*s != 0) {
        wait_for_busy_flag(handle);
        LCD_RESULT r = write_data(handle, *s);
        if (r != LCD_OK) return r;
        s++;
    }
    return LCD_OK;
}

LCD_RESULT lcd_clear_display(hd44780* handle) {
    return write_cmd(handle, 0x01);
}

LCD_RESULT lcd_set_led(hd44780* handle, bool on) {
    state_write_bit(handle, on, LCD_PIN_LED);
    GPIO_WRITE();
    return LCD_OK;
}

LCD_RESULT lcd_display_ctrl(hd44780* handle, bool display_on, bool cursor_on,
                            bool cursor_blinking) {
    uint8_t cmd = 0x08 | (display_on << 2) | (cursor_on << 1) | cursor_blinking;
    return write_cmd(handle, cmd);
}

LCD_RESULT lcd_shift_cursor(hd44780* handle, LCD_DIRECTION direction,
                            uint8_t steps) {
    uint8_t cmd = 0x10 | (direction << 2);
    for (int i = 0; i < steps; i++) {
        LCD_RESULT r = write_cmd(handle, cmd);
        if (r != LCD_OK) return r;
    }
    return LCD_OK;
}

LCD_RESULT lcd_shift_display(hd44780* handle, LCD_DIRECTION direction,
                             uint8_t steps) {
    uint8_t cmd = 0x18 | (direction << 2);
    for (int i = 0; i < steps; i++) {
        LCD_RESULT r = write_cmd(handle, cmd);
        if (r != LCD_OK) return r;
    }
    return LCD_OK;
}

LCD_RESULT lcd_write_number(hd44780* handle, unsigned long n, uint8_t base) {
    char  buf[21];
    char* str = &buf[20];
    *str      = '\0';

    if (base < 2) return LCD_ERROR;

    do {
        char c = n % base;
        *--str = c < 10 ? c + '0' : c + 'A' - 10;
        n /= base;
    } while (n);
    return lcd_write_string(handle, str);
}

LCD_RESULT lcd_write_float(hd44780* handle, double number, uint8_t digits) {
    // Handle negative numbers
    if (number < 0.0) {
        lcd_write_string(handle, "-");
        number = -number;
    }

    // Round correctly so that print(1.999, 2) prints as "2.00"
    double rounding = 0.5;
    for (uint8_t i = 0; i < digits; ++i) rounding /= 10.0;
    number += rounding;

    // Extract the integer part of the number and print it
    unsigned long int_part  = (unsigned long)number;
    double        remainder = number - (double)int_part;
    lcd_write_number(handle, int_part, 10);

    if (digits > 0) {
        lcd_write_string(handle, ".");
        // Print the remainder
        lcd_write_number(handle, remainder * pow(10, digits), 10);
    }

    return LCD_OK;
}

LCD_RESULT lcd_set_entry_mode(hd44780* handle, LCD_DIRECTION_INC_DEC direction,
                              LCD_SHIFT shift) {
    uint8_t cmd = 0x04 | (direction << 1) | shift;
    return write_cmd(handle, cmd);
}
