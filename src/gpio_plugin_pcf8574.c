#include "hd44780.h"
#include "pcf8574.h"

static void write(uint8_t data, void* pcf) {
    pcf8574_write((pcf8574*)pcf, data);
}

static uint8_t read(void* pcf) {
    uint8_t data = 0;
    pcf8574_read((pcf8574*)pcf, &data);
    return data;
}

gpio_plugin lcd_plugin_pcf8574_init(pcf8574* pcf) {
    return (gpio_plugin){write, read, pcf};
}
