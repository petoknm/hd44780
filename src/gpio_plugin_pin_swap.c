#include "gpio_plugin_pin_swap.h"

static void write(uint8_t data, void* config) {
    pin_swap_config* cfg     = (pin_swap_config*)config;
    uint8_t          swapped = 0;
    for (int i = 0; i < 8; i++) {
        if (data & (1 << i)) { swapped |= 1 << cfg->forward[i]; }
    }
    cfg->plugin.write(swapped, cfg->plugin.data);
}

static uint8_t read(void* config) {
    pin_swap_config* cfg     = (pin_swap_config*)config;
    uint8_t          swapped = cfg->plugin.read(cfg->plugin.data);
    uint8_t          data    = 0;
    for (int i = 0; i < 8; i++) {
        if (swapped & (1 << i)) { data |= 1 << cfg->backward[i]; }
    }
    return data;
}

pin_swap_config lcd_plugin_pin_swap_create(gpio_plugin plugin,
                                           LCD_PIN*    layout) {
    pin_swap_config cfg;
    cfg.plugin = plugin;
    for (int i = 0; i < 8; i++) {
        cfg.backward[i]        = layout[i];
        cfg.forward[layout[i]] = i;
    }
    return cfg;
}

gpio_plugin lcd_plugin_pin_swap(pin_swap_config* config) {
    return (gpio_plugin){write, read, (void*)config};
}
