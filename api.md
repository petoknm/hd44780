# Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`enum `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)            | Possible return values for the functions
`enum `[`LCD_PIN`](#hd44780_8h_1a3f1ddbaa6e59d522eb1b30e22e7ae4a1)            | Enumeration of the LCD pins
`enum `[`LCD_DATA_LENGTH`](#hd44780_8h_1a435e7172e4ebd8c0a7f3541483986231)            | 
`enum `[`LCD_DISPLAY_LINES`](#hd44780_8h_1a5af41dfbff0199a297d8025857393d27)            | 
`enum `[`LCD_FONT`](#hd44780_8h_1a9d132e2cf3ef7204c7bad0d7c8d86eed)            | 
`enum `[`LCD_DIRECTION`](#hd44780_8h_1a01c5ec9ee55670d7f7202917a62dc7a4)            | LCD direction
`enum `[`LCD_DIRECTION_INC_DEC`](#hd44780_8h_1a5b3b2dd4970e4e42520acbe1c6ea6688)            | LCD direction increment
`enum `[`LCD_SHIFT`](#hd44780_8h_1aeaaee990fffdadef85abfcf53d3288a0)            | LCD shift
`public `[`gpio_plugin`](#structgpio__plugin)` `[`lcd_plugin_direct`](#gpio__plugin__direct_8h_1ab8f98eb196e657b9ce3b480f5113f6d4)`(GPIO_TypeDef * port)`            | 
`public `[`gpio_plugin`](#structgpio__plugin)` `[`lcd_plugin_pcf8574`](#gpio__plugin__pcf8574_8h_1a3c0e39fc73222c2f0029db6024932247)`(pcf8574 * pcf)`            | 
`public `[`pin_swap_config`](#structpin__swap__config)` `[`lcd_plugin_pin_swap_create`](#gpio__plugin__pin__swap_8h_1a1a2a846aed2ea03d074e922f489d73d9)`(`[`gpio_plugin`](#structgpio__plugin)` plugin,`[`LCD_PIN`](#hd44780_8h_1a3f1ddbaa6e59d522eb1b30e22e7ae4a1)` * layout)`            | Creates a pin swap configuration needed for the plugin 
`public `[`gpio_plugin`](#structgpio__plugin)` `[`lcd_plugin_pin_swap`](#gpio__plugin__pin__swap_8h_1ad03cd2524a312e2e431e4be41ae780c4)`(`[`pin_swap_config`](#structpin__swap__config)` * config)`            | Creates a swap plugin from a configuration
`public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_init`](#hd44780_8h_1a9a4c24d8fd592e3131c33314dfaf4e58)`(`[`hd44780_config`](#structhd44780__config)` * config,`[`hd44780`](#structhd44780)` * handle)`            | LCD initialization function 
`public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_set_location`](#hd44780_8h_1a637c6c9914248879ec4d176041c2a6cf)`(`[`hd44780`](#structhd44780)` * handle,uint8_t x,uint8_t y)`            | Sets the location of the memory pointer in the controller (used to control other operations (for example where to write a string)) 
`public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_write_string`](#hd44780_8h_1acc45644f465341c827892ba7ae27a84a)`(`[`hd44780`](#structhd44780)` * handle,char * s)`            | Writes a string to the LCD 
`public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_clear_display`](#hd44780_8h_1a9b32c749556d48befcc5f2b9c4551bbd)`(`[`hd44780`](#structhd44780)` * handle)`            | Clears the LCD 
`public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_set_led`](#hd44780_8h_1a1c2efd9f44582ab4d8470d2b59671110)`(`[`hd44780`](#structhd44780)` * handle,bool on)`            | Controls the state of the LCD backlight 
`public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_display_ctrl`](#hd44780_8h_1a732b0b33c86f8ad2febbd5db38d804bf)`(`[`hd44780`](#structhd44780)` * handle,bool display_on,bool cursor_on,bool cursor_blinking)`            | 
`public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_shift_cursor`](#hd44780_8h_1adf9b7ae9ee920bc121b9a8e46aa76b4e)`(`[`hd44780`](#structhd44780)` * handle,`[`LCD_DIRECTION`](#hd44780_8h_1a01c5ec9ee55670d7f7202917a62dc7a4)` direction,uint8_t steps)`            | Shifts the cursor in the specified direction certain number of steps 
`public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_shift_display`](#hd44780_8h_1a798d969438a1f4547888bc2e6a42c7fa)`(`[`hd44780`](#structhd44780)` * handle,`[`LCD_DIRECTION`](#hd44780_8h_1a01c5ec9ee55670d7f7202917a62dc7a4)` direction,uint8_t steps)`            | Shifts the contents of the LCD 
`public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_write_number`](#hd44780_8h_1abbad9490610ec6cf525994045b0e530b)`(`[`hd44780`](#structhd44780)` * handle,unsigned long n,uint8_t base)`            | Writes a number to the LCD 
`public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_write_float`](#hd44780_8h_1a48e12467f78cd181d068798bb9a264dd)`(`[`hd44780`](#structhd44780)` * handle,double number,uint8_t digits)`            | 
`public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_set_entry_mode`](#hd44780_8h_1a9f7d036766611bf59117995f73f05e30)`(`[`hd44780`](#structhd44780)` * handle,`[`LCD_DIRECTION_INC_DEC`](#hd44780_8h_1a5b3b2dd4970e4e42520acbe1c6ea6688)` direction,`[`LCD_SHIFT`](#hd44780_8h_1aeaaee990fffdadef85abfcf53d3288a0)` shift)`            | Sets the mode by which data is written to the LCD 
`struct `[`gpio_plugin`](#structgpio__plugin) | GPIO plugin definition
`struct `[`hd44780`](#structhd44780) | LCD handle structure
`struct `[`hd44780_config`](#structhd44780__config) | LCD configuration structure
`struct `[`pin_swap_config`](#structpin__swap__config) | 

## Members

#### `enum `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0) 

 Values                         | Descriptions                                
--------------------------------|---------------------------------------------
LCD_OK            | Function call was successful
LCD_TIMEOUT            | Function call timed out
LCD_ERROR            | Function call was unsuccessful

Possible return values for the functions

#### `enum `[`LCD_PIN`](#hd44780_8h_1a3f1ddbaa6e59d522eb1b30e22e7ae4a1) 

 Values                         | Descriptions                                
--------------------------------|---------------------------------------------
LCD_PIN_D4            | 
LCD_PIN_D5            | 
LCD_PIN_D6            | 
LCD_PIN_D7            | 
LCD_PIN_RS            | 
LCD_PIN_RW            | 
LCD_PIN_E            | 
LCD_PIN_LED            | 

Enumeration of the LCD pins

#### `enum `[`LCD_DATA_LENGTH`](#hd44780_8h_1a435e7172e4ebd8c0a7f3541483986231) 

 Values                         | Descriptions                                
--------------------------------|---------------------------------------------
DATA_LENGTH_4            | 
DATA_LENGHT_8            | 

#### `enum `[`LCD_DISPLAY_LINES`](#hd44780_8h_1a5af41dfbff0199a297d8025857393d27) 

 Values                         | Descriptions                                
--------------------------------|---------------------------------------------
DISPLAY_LINES_1            | 
DISPLAY_LINES_2            | 

#### `enum `[`LCD_FONT`](#hd44780_8h_1a9d132e2cf3ef7204c7bad0d7c8d86eed) 

 Values                         | Descriptions                                
--------------------------------|---------------------------------------------
FONT_5x8            | 
FONT_5x10            | 

#### `enum `[`LCD_DIRECTION`](#hd44780_8h_1a01c5ec9ee55670d7f7202917a62dc7a4) 

 Values                         | Descriptions                                
--------------------------------|---------------------------------------------
DIRECTION_LEFT            | 
DIRECTION_RIGHT            | 

LCD direction

#### `enum `[`LCD_DIRECTION_INC_DEC`](#hd44780_8h_1a5b3b2dd4970e4e42520acbe1c6ea6688) 

 Values                         | Descriptions                                
--------------------------------|---------------------------------------------
DIRECTION_INCREMENT            | 
DIRECTION_DECREMENT            | 

LCD direction increment

#### `enum `[`LCD_SHIFT`](#hd44780_8h_1aeaaee990fffdadef85abfcf53d3288a0) 

 Values                         | Descriptions                                
--------------------------------|---------------------------------------------
SHIFT_YES            | 
SHIFT_NO            | 

LCD shift

#### `public `[`gpio_plugin`](#structgpio__plugin)` `[`lcd_plugin_direct`](#gpio__plugin__direct_8h_1ab8f98eb196e657b9ce3b480f5113f6d4)`(GPIO_TypeDef * port)` 

#### `public `[`gpio_plugin`](#structgpio__plugin)` `[`lcd_plugin_pcf8574`](#gpio__plugin__pcf8574_8h_1a3c0e39fc73222c2f0029db6024932247)`(pcf8574 * pcf)` 

#### `public `[`pin_swap_config`](#structpin__swap__config)` `[`lcd_plugin_pin_swap_create`](#gpio__plugin__pin__swap_8h_1a1a2a846aed2ea03d074e922f489d73d9)`(`[`gpio_plugin`](#structgpio__plugin)` plugin,`[`LCD_PIN`](#hd44780_8h_1a3f1ddbaa6e59d522eb1b30e22e7ae4a1)` * layout)` 

Creates a pin swap configuration needed for the plugin 
#### Parameters
* `plugin` plugin to wrap 

* `layout` new LCD_PIN layout (pointer to 8 LCD_PINs) 

#### Returns
pin swap configuration

#### `public `[`gpio_plugin`](#structgpio__plugin)` `[`lcd_plugin_pin_swap`](#gpio__plugin__pin__swap_8h_1ad03cd2524a312e2e431e4be41ae780c4)`(`[`pin_swap_config`](#structpin__swap__config)` * config)` 

Creates a swap plugin from a configuration

#### `public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_init`](#hd44780_8h_1a9a4c24d8fd592e3131c33314dfaf4e58)`(`[`hd44780_config`](#structhd44780__config)` * config,`[`hd44780`](#structhd44780)` * handle)` 

LCD initialization function 
#### Parameters
* `config` - a pointer to the LCD configuration structure 

* `handle` - a pointer to the LCD handle 

#### Returns
whether the function was successful or not

#### `public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_set_location`](#hd44780_8h_1a637c6c9914248879ec4d176041c2a6cf)`(`[`hd44780`](#structhd44780)` * handle,uint8_t x,uint8_t y)` 

Sets the location of the memory pointer in the controller (used to control other operations (for example where to write a string)) 
#### Parameters
* `handle` - a pointer to the LCD handle 

* `x` - x-coordinate of the location 

* `y` - y-coordinate of the location 

#### Returns
whether the function was successful or not

#### `public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_write_string`](#hd44780_8h_1acc45644f465341c827892ba7ae27a84a)`(`[`hd44780`](#structhd44780)` * handle,char * s)` 

Writes a string to the LCD 
#### Parameters
* `handle` - a pointer to the LCD handle 

* `s` - string you want to write to the LCD 

#### Returns
whether the function was successful or not

#### `public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_clear_display`](#hd44780_8h_1a9b32c749556d48befcc5f2b9c4551bbd)`(`[`hd44780`](#structhd44780)` * handle)` 

Clears the LCD 
#### Parameters
* `handle` - a pointer to the LCD handle 

#### Returns
whether the function was successful or not

#### `public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_set_led`](#hd44780_8h_1a1c2efd9f44582ab4d8470d2b59671110)`(`[`hd44780`](#structhd44780)` * handle,bool on)` 

Controls the state of the LCD backlight 
#### Parameters
* `handle` - a pointer to the LCD handle 

* `on` - set it to 1 if you want to turn the backlight on, else 0 

#### Returns
whether the function was successful or not

#### `public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_display_ctrl`](#hd44780_8h_1a732b0b33c86f8ad2febbd5db38d804bf)`(`[`hd44780`](#structhd44780)` * handle,bool display_on,bool cursor_on,bool cursor_blinking)` 

#### `public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_shift_cursor`](#hd44780_8h_1adf9b7ae9ee920bc121b9a8e46aa76b4e)`(`[`hd44780`](#structhd44780)` * handle,`[`LCD_DIRECTION`](#hd44780_8h_1a01c5ec9ee55670d7f7202917a62dc7a4)` direction,uint8_t steps)` 

Shifts the cursor in the specified direction certain number of steps 
#### Parameters
* `handle` - a pointer to the LCD handle 

* `direction` - specifies the direction 

* `steps` - specifies how many positions to shift the cursor by 

#### Returns
whether the function was successful or not

#### `public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_shift_display`](#hd44780_8h_1a798d969438a1f4547888bc2e6a42c7fa)`(`[`hd44780`](#structhd44780)` * handle,`[`LCD_DIRECTION`](#hd44780_8h_1a01c5ec9ee55670d7f7202917a62dc7a4)` direction,uint8_t steps)` 

Shifts the contents of the LCD 
#### Parameters
* `handle` - a pointer to the LCD handle 

* `direction` - directions of the shift 

* `steps` - how many positions to shift the contents by 

#### Returns
whether the function was successful or not

#### `public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_write_number`](#hd44780_8h_1abbad9490610ec6cf525994045b0e530b)`(`[`hd44780`](#structhd44780)` * handle,unsigned long n,uint8_t base)` 

Writes a number to the LCD 
#### Parameters
* `handle` - a pointer to the LCD handle 

* `n` - a number you want to write to the LCD 

* `base` - numeric base to display the number in 

#### Returns
whether the function was successful or not

#### `public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_write_float`](#hd44780_8h_1a48e12467f78cd181d068798bb9a264dd)`(`[`hd44780`](#structhd44780)` * handle,double number,uint8_t digits)` 

#### `public `[`LCD_RESULT`](#hd44780_8h_1addc817fafbc1e995a7d4c229c65dd6c0)` `[`lcd_set_entry_mode`](#hd44780_8h_1a9f7d036766611bf59117995f73f05e30)`(`[`hd44780`](#structhd44780)` * handle,`[`LCD_DIRECTION_INC_DEC`](#hd44780_8h_1a5b3b2dd4970e4e42520acbe1c6ea6688)` direction,`[`LCD_SHIFT`](#hd44780_8h_1aeaaee990fffdadef85abfcf53d3288a0)` shift)` 

Sets the mode by which data is written to the LCD 
#### Parameters
* `handle` - a pointer to the LCD handle 

* `direction` 

* `shift` 

#### Returns
whether the function was successful or not

# struct `gpio_plugin` 

GPIO plugin definition

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public void(* `[`write`](#structgpio__plugin_1ae359b9080fa0a934253e18b88b53f6ae) | 
`public uint8_t(* `[`read`](#structgpio__plugin_1af6edf256fb64feee5c67bb9d9ecde5e5) | 
`public void * `[`data`](#structgpio__plugin_1a735984d41155bc1032e09bece8f8d66d) | 

## Members

#### `public void(* `[`write`](#structgpio__plugin_1ae359b9080fa0a934253e18b88b53f6ae) 

#### `public uint8_t(* `[`read`](#structgpio__plugin_1af6edf256fb64feee5c67bb9d9ecde5e5) 

#### `public void * `[`data`](#structgpio__plugin_1a735984d41155bc1032e09bece8f8d66d) 

# struct `hd44780` 

LCD handle structure

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public uint8_t `[`state`](#structhd44780_1a0b57aa10271a66f3dc936bba1d2f3830) | 
`public `[`hd44780_config`](#structhd44780__config)` `[`config`](#structhd44780_1abef3a74a3b7f32d8164984aca2e9de6f) | 

## Members

#### `public uint8_t `[`state`](#structhd44780_1a0b57aa10271a66f3dc936bba1d2f3830) 

#### `public `[`hd44780_config`](#structhd44780__config)` `[`config`](#structhd44780_1abef3a74a3b7f32d8164984aca2e9de6f) 

# struct `hd44780_config` 

LCD configuration structure

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public uint8_t `[`lines`](#structhd44780__config_1a8935fe41864f4737660ea9c219cfa2cb) | 
`public uint32_t `[`timeout`](#structhd44780__config_1ab5627d8d8b095c198e2523c44ca380ac) | 
`public `[`gpio_plugin`](#structgpio__plugin)` `[`gpio`](#structhd44780__config_1a3155b8e343bbf49a3aca8f97c954f7a7) | 

## Members

#### `public uint8_t `[`lines`](#structhd44780__config_1a8935fe41864f4737660ea9c219cfa2cb) 

#### `public uint32_t `[`timeout`](#structhd44780__config_1ab5627d8d8b095c198e2523c44ca380ac) 

#### `public `[`gpio_plugin`](#structgpio__plugin)` `[`gpio`](#structhd44780__config_1a3155b8e343bbf49a3aca8f97c954f7a7) 

# struct `pin_swap_config` 

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public `[`gpio_plugin`](#structgpio__plugin)` `[`plugin`](#structpin__swap__config_1a6d224cc43af48334cc59dcea0b57b0f4) | 
`public `[`LCD_PIN`](#hd44780_8h_1a3f1ddbaa6e59d522eb1b30e22e7ae4a1)` `[`forward`](#structpin__swap__config_1a064a32f9d4464e2cb29a6af0d067ccd4) | Mapping from the library layout to custom one. Indices are LCD_PINs from the library layout. Values are corresponding LCD_PINs in the custom layout.
`public `[`LCD_PIN`](#hd44780_8h_1a3f1ddbaa6e59d522eb1b30e22e7ae4a1)` `[`backward`](#structpin__swap__config_1a3d76850702b3bb13eff9ade7a4c72f3e) | Mapping from custom layout to the library one. Indices are LCD_PINs from the custom layout. Values are corresponding LCD_PINs in the library layout.

## Members

#### `public `[`gpio_plugin`](#structgpio__plugin)` `[`plugin`](#structpin__swap__config_1a6d224cc43af48334cc59dcea0b57b0f4) 

#### `public `[`LCD_PIN`](#hd44780_8h_1a3f1ddbaa6e59d522eb1b30e22e7ae4a1)` `[`forward`](#structpin__swap__config_1a064a32f9d4464e2cb29a6af0d067ccd4) 

Mapping from the library layout to custom one. Indices are LCD_PINs from the library layout. Values are corresponding LCD_PINs in the custom layout.

#### `public `[`LCD_PIN`](#hd44780_8h_1a3f1ddbaa6e59d522eb1b30e22e7ae4a1)` `[`backward`](#structpin__swap__config_1a3d76850702b3bb13eff9ade7a4c72f3e) 

Mapping from custom layout to the library one. Indices are LCD_PINs from the custom layout. Values are corresponding LCD_PINs in the library layout.

Generated by [Moxygen](https://sourcey.com/moxygen)